﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CorpseLoot : MonoBehaviour {
	public LayerMask collisionMask;

	private Rect inventoryRect = new Rect(300, 100, 400, 400);
	private bool inventoryShow = false;
	//public Transform player;

	private Dictionary<int, string> lootDictionary = new Dictionary<int, string>() {
		{0, string.Empty},
		{1, string.Empty},
		{2, string.Empty},
		{3, string.Empty},
		{4, string.Empty},
		{5, string.Empty},
	};

	private Ray mouseRay;
	private RaycastHit hit;
	private RaycastHit targetRayHit;
	private Vector3 playerPosition;
	private Vector3 playerTargedDirection;

	// Use this for initialization
	void Start () {
		playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
		//lootDictionary[0] = itemObject.pistolItem.name;
		//lootDictionary[1] = itemObject.arItem.name;
	}
	
	// Update is called once per frame
	void Update () { 
		mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
		playerTargedDirection = hit.point - playerPosition;
		
		if (Physics.Raycast(mouseRay.origin, mouseRay.direction, out hit)) { //Get the data of the location it hit
			if (Input.GetButton ("Fire2")) {
				if (Physics.Raycast(playerPosition, playerTargedDirection.normalized, out targetRayHit, 1, collisionMask)) {
					if (targetRayHit.collider.transform.tag == "LootableCorpse") {
						inventoryShow = true;
						Debug.DrawRay(playerPosition, playerTargedDirection.normalized * 5, Color.white);
					}
				}
				Debug.DrawRay(playerPosition, playerTargedDirection.normalized * 500, Color.white);
				Debug.DrawRay(mouseRay.origin, mouseRay.direction * 50, Color.red);
			}
		}


		if (Input.GetKeyDown("l")) {
			inventoryShow = false;	
		}
	}

	void OnGUI() {		
		if(inventoryShow) {
			inventoryRect = GUI.Window(0,inventoryRect, InventoryMethod, "Corpse"); //Creates the inventory window with the Rect sizes and the Method (buttons, etc)
		}
	}
	
	void InventoryMethod(int windowID) {		
		GUILayout.BeginArea (new Rect (5, 50, 400, 400)); //The starting are of the Layout, 50px down so we don't end up drawing on the window's title area
		
		GUILayout.BeginHorizontal();
		if (GUILayout.Button (lootDictionary [0], GUILayout.Height (50))) {
			if(lootDictionary[0] != string.Empty) {
				InventoryGUI.inventoryNameDictionary[0] = lootDictionary[0];
				lootDictionary[0] = string.Empty;
			}
		}
		if (GUILayout.Button(lootDictionary[1], GUILayout.Height (50))) {
			if(lootDictionary[1] != string.Empty) {
				InventoryGUI.inventoryNameDictionary[1] = lootDictionary[1];
				lootDictionary[1] = string.Empty;
			}
		}
		if (GUILayout.Button(lootDictionary[2], GUILayout.Height (50))) {
			if(lootDictionary[2] != string.Empty) {
				InventoryGUI.inventoryNameDictionary[2] = lootDictionary[2];
				lootDictionary[2] = string.Empty;
			}
		}
		GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		if (GUILayout.Button(lootDictionary[3], GUILayout.Height (50))) {
			if(lootDictionary[3] != string.Empty) {
				InventoryGUI.inventoryNameDictionary[3] = lootDictionary[3];
				lootDictionary[3] = string.Empty;
			}
		}
		if (GUILayout.Button(lootDictionary[4], GUILayout.Height (50))) {
			if(lootDictionary[4] != string.Empty) {
				InventoryGUI.inventoryNameDictionary[4] = lootDictionary[4];
				lootDictionary[4] = string.Empty;
			}
		}
		if (GUILayout.Button(lootDictionary[5], GUILayout.Height (50))) {
			if(lootDictionary[5] != string.Empty) {
				InventoryGUI.inventoryNameDictionary[5] = lootDictionary[5];
				lootDictionary[5] = string.Empty;
			}
		}
		GUILayout.EndHorizontal();
		
		GUILayout.EndArea ();
	}
}
