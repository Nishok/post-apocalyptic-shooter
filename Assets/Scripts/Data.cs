﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Data : MonoBehaviour {

	public static Data data; //So we can access the data from everywhere without using GameObject.Find()

	public List<Items> inventory;
	public Items[] items;

	// Use this for initialization
	void Awake () {
		if(data == null) { //If data does not exist, give the gameObject the data and prevent destroying on scene changes. If data exist and it isn't this data, destroy this gameObject because we don't want 2 copies.
			DontDestroyOnLoad(gameObject);
			data = this;
		} 
		else if(data != this) {
			Destroy(gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void checkInventoryDuplicates() {
		for(int i = 0; i < Data.data.inventory.Count; i++) {
			for(int j = 0; j < Data.data.inventory.Count; j++) {
				if(Data.data.inventory[i].name == Data.data.inventory[j].name && i != j) {
					Data.data.inventory[i].amount += Data.data.inventory[j].amount;
					Data.data.inventory.RemoveAt(j);
					j--;
				}
			}
		}
	}
}
