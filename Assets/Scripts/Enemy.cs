﻿using UnityEngine;
using System.Collections;

public class Enemy : Entity {

	public float expOnDeath;
	private PlayerController player;

	void Start() {
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
	}

	public override void Die ()
	{
		player.AddExp(expOnDeath);
		base.Die();
	}
}
