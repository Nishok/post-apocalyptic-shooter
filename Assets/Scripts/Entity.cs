﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour {

	public float health;
	public int level;
	public float currentLevelExp;
	public float expToLevel;
	public float rotationSpeed = 450;
	public float walkSpeed = 5;
	public float runSpeed = 8;
	public float acceleration = 5;
	//private float acceleration = 5;
	//public string name;


	public virtual void TakeDamage(float dmg) {
		health -= dmg;

		if(health <= 0) {
			Die();
		}
	}

	public virtual void Die() {
		Destroy(gameObject);
	}
}
