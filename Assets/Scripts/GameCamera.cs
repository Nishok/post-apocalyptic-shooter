﻿using System;
using UnityEngine;

public class GameCamera : MonoBehaviour {

    //Using a Func so if I ever decide to change what the camera should follow, I can just do SetFollowPosition(() => myVector3).
    private Func<Vector3> CameraPositionFunc;

    public GameObject player;
    public float smoothing = 5f;
    public float zoom = 60f;

    public Vector3 offset;
    Camera mainCamera;

    void Start()
    {
        mainCamera = Camera.main;
        CameraPositionFunc = () => player.transform.position;
        offset = new Vector3(CameraPositionFunc().x, 10f, -5f);
    } 

    // LateUpdate is called after Update each frame
    void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, CameraPositionFunc() + offset, smoothing * Time.deltaTime);

        HandleZoom();
    }

    public void SetFollowPosition(Func<Vector3> cameraPositionFunc)
    {
        CameraPositionFunc = cameraPositionFunc;
    }

    private void HandleZoom()
    {
        //float zoom = 40f;

        float zoomAmt = 80f;
        if(Input.GetKey(KeyCode.KeypadPlus))
        {
            zoom -= zoomAmt * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            zoom += zoomAmt * Time.deltaTime;
        }

        if (Input.mouseScrollDelta.y > 0)
        {
            zoom -= zoomAmt * Time.deltaTime * 10f;
        }

        if (Input.mouseScrollDelta.y < 0)
        {
            zoom += zoomAmt * Time.deltaTime * 10f;
        }

        zoom = Mathf.Clamp(zoom, 20f, 60f);

        float zoomDifference = zoom - mainCamera.fieldOfView;
        mainCamera.fieldOfView += zoomDifference * smoothing * Time.deltaTime;
    }
}
