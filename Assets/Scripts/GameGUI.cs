﻿using UnityEngine;
using System.Collections;

public class GameGUI : MonoBehaviour {

	public Transform expirienceBar;
	public TextMesh levelText;
	public TextMesh ammoText;

	public void SetPlayerExp(float percentToLevel, int playerLevel) {
		levelText.text = "level: " + playerLevel; //Set the level's text
		expirienceBar.localScale = new Vector3(percentToLevel, 1, 1); //Set the EXP bar's amount
	}

	public void SetAmmoInfo(int currentAmmo, int totalAmmo) {
		ammoText.text = currentAmmo + " / " + totalAmmo;
	}
}
