﻿using System.Collections;
using UnityEngine;

[RequireComponent (typeof (AudioSource))]

public class Gun : MonoBehaviour {


	public enum GunType {Semi, Burts, Auto};

	public LayerMask collisionMask;
	public float gunID;
	public GunType gunType;

	public int currentGunID;
	public int currentGun;
	public int currentAmmo; //Data.data.inventory[currentAmmoID].amount = the totalAmmo
	public int currentAmmoID;

	public Transform spawn;
	public Transform shellEjectionPoint;
	public Rigidbody shell;
	private LineRenderer tracer;
	private Ray mouseRay;
	private RaycastHit mouseHit;
	public LayerMask aimLayer;

	private Camera cam;

	[HideInInspector]
	public GameGUI gui;

	private float secondBetweenShots;
	private float nextPossibleShootTime;
	private bool reloading;

	private Items gun;
	private Items ammo;

	void Start() {
		if(GetComponent<LineRenderer>()) { //If the component (weapon) has a tracer (ranged weps)
			tracer = GetComponent<LineRenderer>();
		}

		cam = Camera.main;

		//currentMagAmmo = magAmmo;

		for(int i = 0; i < Data.data.inventory.Count; i++) {
			if(currentGunID == Data.data.inventory[i].id) {
				currentGun = i;
				gun = Data.data.inventory[currentGun];
				for(int j = 0; j < Data.data.inventory.Count; j++) {
					if(Data.data.inventory[j].type.ToString() == "ammo" && gun.ammotype.ToString() == Data.data.inventory[j].name) {
						currentAmmo = Data.data.inventory[j].amount;
						currentAmmoID = j;
						break;
					}
					else {
						currentAmmo = 0;
					}
				}
			}
		}
		secondBetweenShots = 60 / gun.rpm;

		ammo = Data.data.inventory[currentAmmoID];

		if(gui) {
			gui.SetAmmoInfo(gun.magAmmo, currentAmmo);
		}
	}

	void Update() {
		gun = Data.data.inventory[currentGun];
		for(int j = 0; j < Data.data.inventory.Count; j++) {
			if(Data.data.inventory[j].type.ToString() == "ammo" && gun.ammotype.ToString() == Data.data.inventory[j].name) {
				currentAmmo = Data.data.inventory[j].amount;
				currentAmmoID = j;
				break;
			}
			else {
				currentAmmo = 0;
			}
		}

		ammo = Data.data.inventory[currentAmmoID];

		if(gui) {
			gui.SetAmmoInfo(gun.magAmmo, currentAmmo);
		}
	}

	public void Shoot() {
		if (CanShoot()) {
			RaycastHit hit;
			Vector3 mousePos = Input.mousePosition;
			float curPercent = (100 - gun.curDurability) / 15; //Calculates the percent to miss depending on the curdurability amount
			float randomPercent = Random.Range(-curPercent, curPercent); //Randomized the shot's miss-amount depending on the gun's durability
			mousePos += (mousePos * randomPercent) / 100; //Calculates the new destination through the miss percentage
			mouseRay = cam.ScreenPointToRay(mousePos);
			Physics.Raycast(mouseRay, out mouseHit, Mathf.Infinity, aimLayer);
			Vector3 spawnTargetDirection = mouseHit.point - spawn.position;
			Ray ray = new Ray (spawn.position, spawnTargetDirection.normalized);

			//Debug.Log ("Normal: " + Input.mousePosition);
			//Debug.Log ("RandomNormal: " + mousePos);
			//Debug.Log("curPercent: " + curPercent);


			float shotDistance = 20;

			if (Physics.Raycast(ray, out hit, shotDistance, collisionMask)) { //If ray hits something, it gives the ability to modify the hit and give it back to us
				shotDistance = hit.distance;

				if(hit.collider.GetComponent<Entity>()) {
					hit.collider.GetComponent<Entity>().TakeDamage(gun.damage);
				}
			}

			nextPossibleShootTime = Time.time + secondBetweenShots;
			gun.magAmmo--;
			if(Random.Range(0, gun.decayDurability) == 0) { //The chances of the durability decreasing when shooting
				gun.curDurability -= 1;
			}

			if(gui) {
				gui.SetAmmoInfo(gun.magAmmo, currentAmmo);
			}

			GetComponent<AudioSource>().Play();

			if(tracer) {
				StartCoroutine("RenderTracer", ray.direction * shotDistance);
			}

			Rigidbody newShell = Instantiate(shell, shellEjectionPoint.position, Quaternion.identity) as Rigidbody; //Object, Where this object should be, rotation (identity = don't touch rotation)
			newShell.AddForce(shellEjectionPoint.forward * Random.Range(150f, 200f) + spawn.forward * Random.Range(-10f, 10f));

			//Debug.DrawRay(ray.origin, ray.direction * shotDistance, Color.red, 20);
			Debug.DrawRay(spawn.position, spawnTargetDirection.normalized * 500, Color.green, 20);
		}
	}

	public void shootContinuous() {
		if(gunType == GunType.Auto) {
			Shoot();
		}
	}

	private bool CanShoot() {
		bool canShoot = true;

		if(Time.time < nextPossibleShootTime) {
			canShoot = false;
		}

		if(gun.magAmmo <= 0) {
			canShoot = false;
		}

		if(gun.jammed == true) {
			//TODO: Make a jammed sound to notify the player that his gun is still jammed (maybe add a delay so it does not get spammed?)
			Debug.Log("TODO: THE GUN IS STILL JAMMED!");
			canShoot = false;
		}
		
		if(gun.curDurability <= 0) {
			gun.curDurability = 0;
			canShoot = false;
		}
		else if(gun.curDurability <= 25) {
			if(Random.Range(0, gun.curDurability * 4) == 0) {
				//TODO: Jam the gun with sound
				gun.jammed = true;
				Debug.Log("TODO: GUN JAMMED!");
				canShoot = false;
			}
			else if(Random.Range(0, gun.curDurability) == 0) {
				//TODO: Missfire sound
				Debug.Log("TODO: MISSFIRE!");
				canShoot = false;
			}
		}

		if(reloading) {
			canShoot = false;
		}

		return canShoot;
	}

	public bool Reload() {
		if(gun.jammed == true) {
			//TODO: Unjam the weapon with animation and sound
			Debug.Log("TODO: GUN UNJAMMED");
			gun.jammed = false;
			return true;
		}
		else if(currentAmmo != 0 && gun.magAmmo != gun.maxMagAmmo) {
			reloading = true;
			return true;
		}

		return false;
	}

	public void FinishReload() {
		reloading = false;
		int diff = gun.maxMagAmmo - gun.magAmmo;
		if((ammo.amount - diff) < 0) { //Checks if the totalammo is less than the amount of a full mag
			//gun.magAmmo = (gun.maxMagAmmo + (ammo.amount -= (gun.maxMagAmmo - gun.magAmmo))); //Set currentammo to the current mag's ammo amt, then add the addition we have left in our totalammo amt
			gun.magAmmo += ammo.amount;
			ammo.amount = 0;
		} 
		else {
			ammo.amount -= (gun.maxMagAmmo - gun.magAmmo);
			gun.magAmmo = gun.maxMagAmmo;
		}

		gun.curDurability -= Random.Range(1, 4);

		if(gui) {
			gui.SetAmmoInfo(gun.magAmmo, ammo.amount);
		}
	}

	IEnumerator RenderTracer(Vector3 hitPoint) {
		tracer.enabled = true; //Enables the wep's tracer so it shows
		tracer.SetPosition(0, spawn.position); //Set index to 0 and then give it the location of the starting of the tracer
		tracer.SetPosition(1, spawn.position + hitPoint); //The end location of the tracer
		//yield return new WaitForSeconds (10); //This makes it wait 10 seconds basically.
		yield return null; //Waits for a single frame
		tracer.enabled = false;
	}
}
