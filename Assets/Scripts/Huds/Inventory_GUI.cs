﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Inventory_GUI : MonoBehaviour {
	private bool inventoryShow = false;
	private Vector2 scrollBarPos = Vector2.zero;
	//private int gridValue = -1;
	private int currentItem = 0;
	private string itemAmt = "";
	private int ammoAmt = 0;
	private float scrollBarValue = 0;

	private PlayerController playerController;

	public Vector2 windowPosition;
	public Vector2 windowSize;
	public Vector2 closePosition;
	public Vector2 closeSize;
	public Vector2 scrollPosition;
	public Vector2 scrollSize;
	public Vector2 gridPosition;
	public Vector2 gridSize;
	public Vector2 namePosition;
	public Vector2 nameSize;
	public Vector2 itemSmallIconPosition;
	public Vector2 itemSmallIconSize;
	public Vector2 itemBigIconPosition;
	public Vector2 itemBigIconSize;
	public Vector2 itemDescriptionPosition;
	public Vector2 itemDescriptionSize;
	public Vector2 itemWeightPosition;
	public Vector2 itemWeightSize;
	public Vector2 itemValuePosition;
	public Vector2 itemValueSize;
	public Vector2 itemDmgPosition;
	public Vector2 itemDmgSize;
	public Vector2 itemAmmoPosition;
	public Vector2 itemAmmoSize;
	public Vector2 itemDurabilityPosition;
	public Vector2 itemDurabilitySize;
	public Rect dragWindowPosition;

	public Texture inventoryWindow;
	public Texture closeIcon;
	public Texture[] gridIcon;
	public GUISkin skin;

	private GUIStyle headerFont;
	private GUIStyle italicFont;

	// Use this for initialization
	void Start () {
		windowPosition = new Vector2(0, 0);
		windowSize = new Vector2(720, 360);
		closePosition = new Vector2(670, 3);
		closeSize = new Vector2(40, 40);
		scrollPosition = new Vector2(0, 55);
		scrollSize = new Vector2(350, 290);
		gridPosition = new Vector2(10, 2);
		gridSize = new Vector2(323, 410);
		namePosition = new Vector2(320, 5);
		nameSize = new Vector2(250, 250);
		itemSmallIconPosition = new Vector2(10, 0);
		itemSmallIconSize = new Vector2(32, 32);
		itemBigIconPosition = new Vector2(475, 55);
		itemBigIconSize = new Vector2(128, 128);
		itemDescriptionPosition = new Vector2(370, 185);;
		itemDescriptionSize = new Vector2(335, 65);
		itemWeightPosition = new Vector2(windowSize.x - 270, windowSize.y - 100);
		itemWeightSize = new Vector2(65, 20);
		itemValuePosition = new Vector2(windowSize.x - 150, windowSize.y - 100);
		itemValueSize = new Vector2(80, 20);
		itemDmgPosition = new Vector2(windowSize.x - 350, windowSize.y - 75);
		itemDmgSize = new Vector2(65, 20);
		itemAmmoPosition = new Vector2(windowSize.x - 350, windowSize.y - 55);
		itemAmmoSize = new Vector2(145, 20);
		itemDurabilityPosition = new Vector2(windowSize.x - 130, windowSize.y - 25);
		itemDurabilitySize = new Vector2(118, 20);
		dragWindowPosition = new Rect(0, 0, windowSize.x, windowSize.y);
		headerFont = new GUIStyle();
		headerFont.fontSize = 27;
		italicFont = new GUIStyle();
		italicFont.fontStyle = FontStyle.Italic;
		//italicFont.alignment = TextAnchor.MiddleCenter;

		playerController = GameObject.Find("Player").GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp("i")) {
			inventoryShow = !inventoryShow; //Opens or closes the inventory, depending what the previous status was
			scrollBarPos.y = 0; //So the scrollbar does not keep its previous position when re-opened
		}

		if(Input.GetKey("left shift")) {
			if(Input.GetKey("i")) {
				dragWindowPosition.x = 0;
				dragWindowPosition.y = 0;
			}
		}
	}

	void OnGUI() {
		GUI.skin = skin;
		if(inventoryShow == true) {
			dragWindowPosition = GUI.Window(0, dragWindowPosition, InventoryWindow, "");
			if(Event.current.type == EventType.MouseDown && !dragWindowPosition.Contains(Event.current.mousePosition)) { //Closes the inventory when clicked outside
				inventoryShow = false;
			}
		}
	}

	void InventoryWindow(int windowID) {
		Data.data.inventory = Data.data.inventory.OrderBy(x => x.name).ToList();
		GUI.BeginGroup(new Rect(windowPosition.x, windowPosition.y, windowSize.x, windowSize.y), inventoryWindow); //Shows the inventory Texture
		GUI.Label(new Rect(namePosition.x, namePosition.y, nameSize.x, nameSize.y), "Inventory", headerFont);
		if(GUI.Button(new Rect(closePosition.x, closePosition.y, closeSize.x, closeSize.y), closeIcon)) { //Closebutton
			inventoryShow = false;
		}
		scrollBarPos = GUI.BeginScrollView(new Rect(scrollPosition.x, scrollPosition.y, scrollSize.x, scrollSize.y), scrollBarPos, new Rect(0, 0, 0, scrollBarValue));
		itemSmallIconPosition.y = 0;
		for(int i = 0; i < Data.data.inventory.Count; i++) {
			//if(Data.data.inventory[i].type.ToString() == "weapon") {
				if(GUI.Button(new Rect(10, itemSmallIconPosition.y, 325, 32), "")) {
					if(Event.current.button == 0) { //Leftclick
						currentItem = i;
						playerController.EquipGun(i);
					}
					else if(Event.current.button == 1) { //Rightclick
						Debug.Log("Rightbutton");
						Data.data.inventory.RemoveAt(i);
					}
				}
				if(Data.data.inventory[i].amount <= 1) {
					itemAmt = "";
				}
				else {
					itemAmt = "[" + Data.data.inventory[i].amount + "]";
				}
				GUI.Box(new Rect(itemSmallIconPosition.x, itemSmallIconPosition.y, itemSmallIconSize.x, itemSmallIconSize.y), Data.data.inventory[i].icon);
				GUI.Box(new Rect(itemSmallIconPosition.x + 35, itemSmallIconPosition.y, 291, itemSmallIconSize.y), Data.data.inventory[i].name + " " + itemAmt);
				itemSmallIconPosition.y += 35;
				scrollBarValue = itemSmallIconPosition.y;
			//}
		}
		GUI.EndScrollView();
		if(Data.data.inventory.Count >= 1) {
			GUI.Label(new Rect(itemBigIconPosition.x, itemBigIconPosition.y, itemBigIconSize.x, itemBigIconSize.y), Data.data.inventory[currentItem].icon);
			GUI.Label(new Rect(itemDescriptionPosition.x, itemDescriptionPosition.y, itemDescriptionSize.x, itemDescriptionSize.y), "\"" + Data.data.inventory[currentItem].description +"\"", italicFont);
			GUI.Label(new Rect(itemWeightPosition.x, itemWeightPosition.y, itemWeightSize.x, itemWeightSize.y), "WG: " + Data.data.inventory[currentItem].weight + "Kg");
			GUI.Label(new Rect(itemValuePosition.x, itemValuePosition.y, itemValueSize.x, itemValueSize.y), "Value: " + Data.data.inventory[currentItem].value + "");
			if(Data.data.inventory[currentItem].type.ToString() == "weapon") {
				GUI.Label(new Rect(itemDmgPosition.x, itemDmgPosition.y, itemDmgSize.x, itemDmgSize.y), "DMG: " + Data.data.inventory[currentItem].damage + "");
				for(int i = 0; i < Data.data.inventory.Count; i++) {
					if(Data.data.inventory[currentItem].ammotype.ToString() == Data.data.inventory[i].name) {
						ammoAmt = Data.data.inventory[i].amount;
						break;
					}
					else {
						ammoAmt = 0;
					}
				}
				GUI.Label(new Rect(itemAmmoPosition.x, itemAmmoPosition.y, itemAmmoSize.x, itemAmmoSize.y), "Ammo: " + Data.data.inventory[currentItem].ammotype.ToString() + " [" + Data.data.inventory[currentItem].magAmmo + "/" + ammoAmt + "]");
			}
			else if(Data.data.inventory[currentItem].type.ToString() == "ammo") {
				GUI.Label(new Rect(itemDmgPosition.x, itemDmgPosition.y, itemDmgSize.x, itemDmgSize.y), "Amount: " + Data.data.inventory[currentItem].amount + "");
			}
			GUI.Label(new Rect(itemDurabilityPosition.x, itemDurabilityPosition.y, itemDurabilitySize.x, itemDurabilitySize.y), "Durability: " + Data.data.inventory[currentItem].curDurability + "/" + Data.data.inventory[currentItem].totalDurability + "");
		}
		GUI.DragWindow(new Rect (windowPosition.x, windowPosition.y, 10000, 40)); //The last 2 args are for where we can click to drag the window
		GUI.EndGroup();
	}
}
