﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InventoryGUI : MonoBehaviour {
	private bool inventoryShow = false;
	private Rect inventoryRect = new Rect(300, 100, 410, 400); //Use % width/height for resolutions afterwards?!?

	static public Dictionary<int, string> inventoryNameDictionary = new Dictionary<int, string> () {
		{0, string.Empty},
		{1, string.Empty},
		{2, string.Empty},
		{3, string.Empty},
		{4, string.Empty},
		{5, string.Empty}
	};

	//private Items itemObject = new Items(); //Get the items from the Items() class

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI() {
		inventoryShow = GUI.Toggle(new Rect(800, 50, 100, 50), inventoryShow, "Inventory"); //Creates a toggle button

		if(inventoryShow) {
			inventoryRect = GUI.Window(0,inventoryRect, InventoryMethod, "Inventory"); //Creates the inventory window with the Rect sizes and the Method (buttons, etc)
		}
	}

	void InventoryMethod(int windowID) {

		//Display Disctionary
		//inventoryNameDictionary[0] = pistolItem.name;
		//inventoryNameDictionary[1] = arItem.name;


		GUILayout.BeginArea (new Rect (5, 50, 400, 400)); //The starting are of the Layout, 50px down so we don't end up drawing on the window's title area

		GUILayout.BeginHorizontal();
		GUILayout.Button(inventoryNameDictionary[0], GUILayout.Height (50));
		GUILayout.Button(inventoryNameDictionary[1], GUILayout.Height (50));
		GUILayout.Button(inventoryNameDictionary[2], GUILayout.Height (50));
		GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		GUILayout.Button(inventoryNameDictionary[3], GUILayout.Height (50));
		GUILayout.Button(inventoryNameDictionary[4], GUILayout.Height (50));
		GUILayout.Button(inventoryNameDictionary[5], GUILayout.Height (50));
		GUILayout.EndHorizontal();

		GUILayout.EndArea ();
	}
}
