﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable] //So we can see the variables in the inspector of Unity
public class Items {

	public int id;
	public string name;
	public Gun gunPrefab;
	public Texture2D icon;
	public string description;
	public int weight;
	public int damage;
	public int armor;
	public float rpm;
	public int curDurability;
	public int totalDurability;
	public int decayDurability; //The chance amount of which a decay happens when shooting (if 18 then it means 1 out of 18 chance it decays)
	public bool jammed;
	public enum AmmoType {
		none,
		a45,
		a9mm,
		a12gauge
	}
	public int totalAmmo;
	public int magAmmo;
	public int maxMagAmmo;
	public int value;
	public int amount;
	public enum Type {
		misc,
		weapon,
		apparel,
		aid,
		ammo
	}
	public Type type;
	public AmmoType ammotype;

	public Items(int i) {
		id = Data.data.inventory.Count;
		name = Data.data.items[i].name;
		gunPrefab = Data.data.items[i].gunPrefab;
		icon = Data.data.items[i].icon;
		description = Data.data.items[i].description;
		weight = Data.data.items[i].weight;
		damage = Data.data.items[i].damage;
		armor = Data.data.items[i].armor;
		rpm = Data.data.items[i].rpm;
		curDurability = Data.data.items[i].curDurability;
		totalDurability = Data.data.items[i].totalDurability;
		decayDurability = Data.data.items[i].decayDurability;
		jammed = Data.data.items [i].jammed;
		ammotype = Data.data.items[i].ammotype;
		totalAmmo = Data.data.items[i].totalAmmo;
		magAmmo = Data.data.items[i].magAmmo;
		maxMagAmmo = Data.data.items[i].maxMagAmmo;
		value = Data.data.items[i].value;
		amount = Data.data.items[i].amount;
		type = Data.data.items[i].type;
	}
		
	/*public Items(int i, string nam, Texture2D ico, string desc, int weigh, int dmg, int dur, int ammoM) {
		id = i;
		name = nam;
		icon = ico;
		description = desc;
		weight = weigh;
		damage = dmg;
		curDurability = curDurability;
		totalDurability = dur;
		ammoMag = ammoM;
	}*/
}
