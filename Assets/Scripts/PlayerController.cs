﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(CharacterController))]

public class PlayerController : Entity {

	//System
	private Quaternion targetRotation;
	private Vector3 currentVelocityModifier;
	private bool reloading;
	private AnimatorTransitionInfo armsTransitionInfo;

	//Components
	public Transform handHold;
	public Gun[] guns;
	private Gun currentGun;
	
	private Ray mouseRay;
	private RaycastHit mouseHit;
	public LayerMask aimLayer;

	private CharacterController controller;
	private Animator animator;
	private Camera cam;
	private GameGUI gui;

	void Start () {
		controller = GetComponent<CharacterController>();
		gui = GameObject.FindGameObjectWithTag("GUI").GetComponent<GameGUI>();
		cam = Camera.main;
		animator = GetComponent<Animator>();

		//EquipGun(0);
		
		LevelUp ();
	}
	
	// Update is called once per frame
	void Update () {
		ControlMouse();
		//ControlWASD();

		armsTransitionInfo = animator.GetAnimatorTransitionInfo (1);

		//Gun Input
		if(currentGun) {
			if (Input.GetButtonDown("Shoot")) {
					currentGun.Shoot ();
			} else if (Input.GetButton("Shoot")) {
					currentGun.shootContinuous ();
			}

			if(Input.GetButtonDown("Reload")) {
				if(currentGun.Reload()) {
					animator.SetTrigger("Reload"); //Triggers the reload animation
					reloading = true;
				}
			}

			if(reloading) {
				if(armsTransitionInfo.nameHash == Animator.StringToHash("Arms.Reload -> Arms.Weapon Hold")) {
					currentGun.FinishReload();
					reloading = false;
				}
			}
		}

		if(Input.GetKeyUp("[0]")) {
			Data.data.inventory.Add(new Items(0));
		}
		if(Input.GetKeyUp("[1]")) {
			Data.data.inventory.Add(new Items(1));
		}
		if(Input.GetKeyUp("[2]")) {
			Data.data.inventory.Add(new Items(2));
		}
		if(Input.GetKeyUp("[3]")) {
			Data.data.inventory.Add(new Items(3));
		}
		if(Input.GetKeyUp("[9]")) {
			Data.data.checkInventoryDuplicates();
		}
	}

	public void EquipGun(int i) {
		if(currentGun) {
			Destroy(currentGun.gameObject); // Remove the already equipped gun
		}

		//Update the damage, ammo, etc here from the retreived item.//
		currentGun = Instantiate(Data.data.inventory[i].gunPrefab, handHold.position, handHold.rotation) as Gun; //Give the gun the location and rotation of the hand
		currentGun.transform.parent = handHold; //Set the hand as the gun's parent
		currentGun.currentGunID = Data.data.inventory[i].id;
		currentGun.gui = gui;

		animator.SetFloat("Weapon ID", currentGun.gunID); //Set the Weapon ID as the current gun's ID
	}

	void ControlMouse() {
		/*Vector3 playerPos = transform.position;
		Vector3 playerPosScreen = cam.WorldToScreenPoint(playerPos);
		Vector3 facingScreen = mousePos - playerPosScreen;
		Vector3 facingWorld = new Vector3(facingScreen.x, 0, facingScreen.y);
		targetRotation = Quaternion.LookRotation(facingWorld, transform.up);*/
		/*
		Vector3 mousePos = Input.mousePosition; //Gets our mouse's position
		mousePos = cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, cam.transform.position.y - transform.position.y));
		targetRotation = Quaternion.LookRotation(mousePos - new Vector3(transform.position.x, 0, transform.position.z)); //Makes the target (player) look at the mousePos (calculated from the player's position)
		//targetRotation = Quaternion.LookRotation (mousePos - transform.position);
		transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y, targetRotation.eulerAngles.y, rotationSpeed * Time.deltaTime); //smoothens the rotation
		 */

		/*Vector3 mousePos = Input.mousePosition; //Gets our mouse's position
		//Debug.Log ("Mouse: " + mousePos);
		Ray ray = cam.ScreenPointToRay(mousePos);
		Physics.Raycast(ray, out mouseHit, Mathf.Infinity, aimLayer);
		mousePos = mouseHit.point;
		Debug.Log ("mouseHit: " + mousePos);
		targetRotation = Quaternion.LookRotation(mousePos - new Vector3(transform.position.x, 0, transform.position.z)); //Makes the target (player) look at the mousePos (calculated from the player's position)
		transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y, targetRotation.eulerAngles.y, rotationSpeed * Time.deltaTime); //smoothens the rotation
		Debug.DrawRay (transform.position, mousePos * 500, Color.blue);
        */

		Vector3 mousePos = Input.mousePosition; //Gets our mouse's position
		Vector3 playerPos = transform.position;
		//Debug.Log ("Mouse: " + mousePos);
		mouseRay = cam.ScreenPointToRay(mousePos);
		Vector3 playerTargetDirection = mouseHit.point - playerPos;
		//Debug.Log ("mouseHit: " + mouseHit.point);
		Physics.Raycast(mouseRay, out mouseHit, Mathf.Infinity, aimLayer);
		mousePos = mouseHit.point;
		targetRotation = Quaternion.LookRotation(playerTargetDirection.normalized); //Makes the target (player) look at the mousePos (calculated from the player's position)
		transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y, targetRotation.eulerAngles.y, rotationSpeed * Time.deltaTime); //smoothens the rotation
		Debug.DrawRay (playerPos, playerTargetDirection.normalized * 500, Color.blue);

		Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")); //GetAxisRaw goes instantly to 0 or 1, while non-Raw gladually goes to those states.	
		currentVelocityModifier = Vector3.MoveTowards(currentVelocityModifier, input, acceleration * Time.deltaTime);
		Vector3 motion = currentVelocityModifier;
		motion *= (Mathf.Abs(input.x) == 1 && Mathf.Abs(input.z) == 1) ? .7f : 1;
		motion *= (Input.GetButton("Run")) ? runSpeed : walkSpeed;
		motion += Vector3.up * -8; //Gravity
		
		controller.Move(motion * Time.deltaTime);

		animator.SetFloat("Speed", Mathf.Sqrt(motion.x * motion.x + motion.z * motion.z));
	}

	void ControlWASD() {
		Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")); //GetAxisRaw goes instantly to 0 or 1, while non-Raw gladually goes to those states.
		
		if(input != Vector3.zero) { //Makes it so we do not reset our rotation back
			targetRotation = Quaternion.LookRotation(input);
			transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y, targetRotation.eulerAngles.y, rotationSpeed * Time.deltaTime); //smoothens the rotation
		}

		currentVelocityModifier = Vector3.MoveTowards (currentVelocityModifier, input, acceleration * Time.deltaTime);
		Vector3 motion = currentVelocityModifier;
		motion *= (Mathf.Abs(input.x) == 1 && Mathf.Abs(input.z) == 1) ? .7f : 1;
		motion *= (Input.GetButton("Run")) ? runSpeed : walkSpeed;
		motion += Vector3.up * -8; //Gravity
		
		controller.Move(motion * Time.deltaTime);
		
		animator.SetFloat("Speed", Mathf.Sqrt(motion.x * motion.x + motion.z * motion.z));
	}

	public void AddExp(float amt) {
		currentLevelExp += amt;
		if (currentLevelExp >= expToLevel) {
			currentLevelExp -= expToLevel; //So we do not reset our player's EXP on levelup, but instead keep the extra exp for the next level
			LevelUp();
		}
		
		gui.SetPlayerExp(currentLevelExp / expToLevel, level);
	}
	
	public void LevelUp() {
		level++;
		expToLevel = level * 50 + Mathf.Pow(level * 2, 2);
		
		AddExp(0); //This so in case we have enough EXP left to level up again, it runs AddExp() once more to check that
	}
	
	public override void Die ()
	{
		//base.Die(); Runs the code in the func Die()
		health += 1000;
	}
}
